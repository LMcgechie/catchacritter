﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;

namespace Catch_a_Critter
{
    class Critter
    {
        Texture2D Image;
        Vector2 position;
        bool alive;
        int critterValue = 5;
        Score scoreObj = null;
        SoundEffect clickSFX = null;

        public enum animal
        {
            cow,
            dog,
            elephant
        };

        animal i = animal.cow;

        public void SetCritterType(string Inputanimal)
        {
            if (Inputanimal.Equals("cow"))
            {
                i = animal.cow;
            }
            else if (Inputanimal.Equals("dog"))
            {
                i = animal.dog;
            }
            else if (Inputanimal.Equals("elephant"))
            {
                i = animal.elephant;
            }
        }




        public Critter(Score newScore)
        {   
            //Gives access to game score
            scoreObj = newScore;
        }
                       
        public void LoadContent(ContentManager content)
        {
            //Load critter Image
            Image = content.Load<Texture2D>("graphics/cow");
            clickSFX = content.Load<SoundEffect>("audio/buttonclick");
        }


        //Draw Method
        public void Draw(SpriteBatch spriteBatch)
        {        
            if (alive)
                spriteBatch.Draw(Image, position, Color.White);            
        }        

        public void Spawn(GameWindow window)
        {
            alive = true;

            //Location bounds for critter creation
            int xMin = 0,
                yMin = 0,
                xMax = window.ClientBounds.Height - Image.Width,
                yMax = window.ClientBounds.Height - Image.Height;

            //generate random number within bounds
            Random rand;
            rand = new Random();
            position.X = rand.Next(xMin, xMax);
            position.Y = rand.Next(yMin, yMax);
        }

        //Method for "killing" critter
        public void Despawn()
        {
            alive = false;
        }

        public void Input()
        {

            //Get current mouse status
            MouseState currentState = Mouse.GetState();

            //Create hit box for critter
            Rectangle critterBounds = new Rectangle((int)position.X, (int)position.Y, Image.Width, Image.Height);

            //Check for left click in critter hit box
            if (currentState.LeftButton == ButtonState.Pressed && critterBounds.Contains(currentState.X, currentState.Y) && alive == true)
            {
                //Critter clicked
                Despawn();
                scoreObj.IncreaseScore(critterValue);
                clickSFX.Play();    
            }
        }
    }      
}
