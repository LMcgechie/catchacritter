﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace Catch_a_Critter
{
    class Score
    {
        //Data
        int value = 0;
        Vector2 position = new Vector2(10, 10);
        SpriteFont font = null;

        //Behaviour
        public void IncreaseScore(int critterValue)
        {
            value += critterValue;
        }

        public void Draw(SpriteBatch spritebatch)
        {
            //Draw score on screen
            spritebatch.DrawString(font, "Score: " + value, position, Color.Black);
        }

        public void LoadContent(ContentManager content)
        {
            font = content.Load<SpriteFont>("fonts/mainFont");
        }
    }
}
